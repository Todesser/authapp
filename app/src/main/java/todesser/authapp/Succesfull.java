package todesser.authapp;

import android.content.res.ColorStateList;
import android.Manifest;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;

public class Succesfull extends AppCompatActivity implements OnMapReadyCallback{

    //Firebase
    private FirebaseAuth mAuth;
    private FirebaseDatabase db;
    private DatabaseReference dbRef;
    private FirebaseStorage bucket;

    //Google maps
    private GoogleMap mMap;
    private Geocoder mGeocoder;
    public static final double lowerLeftLatitude = 1.396967;
    public static final double lowerLeftLongitude= -78.903968;
    public static final double upperRightLatitude= 11.983639;
    public static final double upperRigthLongitude= -71.869905;

    //UI
    protected TextView display, mail;
    protected EditText address;

    //Estado
    private boolean dia = true;
    LatLng uinicial;
    private List<MarkerOptions> marcadores;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_succesfull);
        //Inicializacion Firebase
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseDatabase.getInstance();
        bucket = FirebaseStorage.getInstance();
        //Inicializacion de variables
        mGeocoder = new Geocoder(getBaseContext());
        marcadores = new ArrayList<>();
        ////////////////////////////////////////////////////////////////////////////7
        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null){
            display = (TextView) findViewById(R.id.successDisplayName);
            display.setTextColor(getResources().getColor(R.color.darkGreen));
            display.setText(user.getDisplayName());
            mail = (TextView) findViewById(R.id.successEmail);
            mail.setText(user.getEmail());
        }
        address = (EditText) findViewById(R.id.successAddress);
        address.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH){
                    String addressString = address.getText().toString();
                    if (!addressString.isEmpty()) {
                        try {
                            List<Address> addresses = mGeocoder.getFromLocationName(
                                    addressString, 2,
                                    lowerLeftLatitude,
                                    lowerLeftLongitude,
                                    upperRightLatitude,
                                    upperRigthLongitude);
                            if (addresses != null && !addresses.isEmpty()) {
                                Address addressResult = addresses.get(0);
                                LatLng position = new LatLng(addressResult.getLatitude(), addressResult.getLongitude());
                                if (mMap != null) {
                                    LatLng pos = new LatLng(addressResult.getLatitude(), addressResult.getLongitude());
                                    mMap.addMarker(new MarkerOptions().position(pos).title(addressString));
                                    address.clearFocus();
                                    trazarRuta(uinicial, position);
                                    Toast.makeText(getBaseContext(), "Dirección añadida al mapa", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(getBaseContext(), "Dirección no encontrada", Toast.LENGTH_SHORT).show();}
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    } else {Toast.makeText(getBaseContext(), "La dirección esta vacía", Toast.LENGTH_SHORT).show();}
                    return true;
                }
                return false;
            }
        });
        mAuth.addAuthStateListener(new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if(mAuth.getCurrentUser()==null){
                    finish();
                }
            }
        });
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void trazarRuta(LatLng inicio, LatLng llegada){
        Routing ruta = new Routing.Builder()
                .travelMode(Routing.TravelMode.DRIVING)
                .withListener(new RoutingListener() {
                    @Override
                    public void onRoutingFailure(RouteException e) {
                        Log.i("Ruta", "Fallo");
                    }

                    @Override
                    public void onRoutingStart() {
                        Log.i("Ruta", "Inicio");
                    }

                    @Override
                    public void onRoutingSuccess(ArrayList<Route> arrayList, int i) {
                        Log.i("Ruta", "Entro en la ruta");
                        Route route = arrayList.get(i);
                        PolylineOptions poly = new PolylineOptions();
                        poly.color(getResources().getColor(R.color.darkGreen));
                        poly.width(10);
                        poly.addAll(route.getPoints());
                        mMap.addPolyline(poly);
                        Toast.makeText(getBaseContext(), "La distancia es de: " + route.getDistanceText(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onRoutingCancelled() {
                        Log.i("Ruta", "Cancelo");
                    }
                })
                .waypoints(inicio, llegada)
                .build();
        ruta.execute();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng bogota = new LatLng(4.60971, -74.08175);
        mMap.addMarker(new MarkerOptions().position(bogota).title("Bogota"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bogota, 12));
        PermissionListener permisos = new PermissionListener() {
            @Override
            public void onPermissionGranted() {
                SmartLocation.with(getBaseContext()).location().oneFix().start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        mMap.clear();
                        dbRef = db.getReference("locations/");
                        dbRef.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for(DataSnapshot snap: dataSnapshot.getChildren()){
                                    Marcador mark = snap.getValue(Marcador.class);
                                    addMarker(new LatLng(mark.getLatitude(), mark.getLongitude()),mark.getName());
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.i("FireDataBase", "Error al leer cambios en los datos");
                            }
                        });
                        LatLng ubicacion = new LatLng(location.getLatitude(), location.getLongitude());
                        uinicial = ubicacion;
                        mMap.addMarker(new MarkerOptions().position(ubicacion).title("Ubicación"));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ubicacion, 15));
                    }
                });
            }

            @Override
            public void onPermissionDenied(ArrayList<String> deniedPermissions) {

            }
        };
        TedPermission.with(getBaseContext())
                .setPermissionListener(permisos)
                .setDeniedMessage("Se necesita acceso a la ubicacion.")
                .setPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                .check();
    }

    private void addMarker(LatLng locacion, String descripcion){
        MarkerOptions mark = new MarkerOptions().position(locacion).title(descripcion);
        marcadores.add(mark);
        mMap.addMarker(mark);
        Log.i("Marcadores", "Marcador Añadido");
    }

    @Override

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.opciones, menu);
        return true;
    }

    protected void logout(){
        mAuth.signOut();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.menulogout:
                logout();
                return true;
            case R.id.menuNight:
                if(dia){
                    mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.night));
                    item.setTitle("Modo Diurno");
                }else{
                    mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.day));
                    item.setTitle("Modo Nocturno");
                }
                dia = !dia;
                return true;
        }
        return false;
    }
}
