package todesser.authapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private EditText userText, passText;
    Pattern emailRegex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d("FAuth", "onAuthStateChanged:signed_in:" + user.getUid());
                    startActivity(new Intent(getBaseContext(), Succesfull.class));
                } else {
                    // User is signed out
                    Log.d("FAuth", "onAuthStateChanged:signed_out");
                }
            }
        };
        userText = (EditText) findViewById(R.id.usertxt);
        passText = (EditText) findViewById(R.id.passtxt);
        emailRegex = Pattern.compile("\\w+@\\w+(.[a-z])+");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(mAuth.getCurrentUser()!=null){
            startActivity(new Intent(getBaseContext(), Succesfull.class));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuthListener != null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    protected void loginPass(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d("FAuth", "signInWithEmail:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w("FAuth", "signInWithEmail:failed", task.getException());
                            Toast.makeText(getBaseContext(), "Fallo la autenticacion", Toast.LENGTH_SHORT).show();
                        }else{
                            userText.setText("");
                            passText.setText("");
                        }
                    }
                });
    }

    public void login(View view){
        String userData = userText.getText().toString();
        String passData = passText.getText().toString();
        Matcher emailMatch = emailRegex.matcher(userData);
        if(emailMatch.matches() && passData.length()>5){
            loginPass(userData, passData);
        }else{
            Toast.makeText(getBaseContext(), "Formato de datos incorrecto.", Toast.LENGTH_SHORT).show();
        }
    }

    public void newUser(View view){
        startActivity(new Intent(getBaseContext(), NewUser.class));
    }
}
