package todesser.authapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NewUser extends AppCompatActivity {

    protected EditText name, last, email, pass;
    protected ImageView img;
    protected Pattern emailRegex;
    protected Bitmap photoBitMap = null;
    //Firebase Initialization
    private FirebaseAuth mAuth;
    private FirebaseStorage bucket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);
        name = (EditText) findViewById(R.id.newNameTxt);
        last = (EditText) findViewById(R.id.newLastTxt);
        email = (EditText) findViewById(R.id.newMailTxt);
        pass = (EditText) findViewById(R.id.newPassTxt);
        img = (ImageView) findViewById(R.id.newPhoto);
        emailRegex = Pattern.compile("\\w+@\\w+(.[a-z])+");
        mAuth = FirebaseAuth.getInstance();
        bucket = FirebaseStorage.getInstance();
    }

    public void register(View view){
        final String nameTxt=name.getText().toString(),
                lastTxt=last.getText().toString(),
                emailTxt=email.getText().toString(),
                passTxt=pass.getText().toString();
        Boolean validMail = emailRegex.matcher(emailTxt).matches();
        if(validMail){
            mAuth.createUserWithEmailAndPassword(emailTxt, passTxt).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        final FirebaseUser user = mAuth.getCurrentUser();
                        if(user!=null){
                            Log.i("Updater", "Aqui esta entrando.");
                            StorageReference photoRef = bucket.getReference("images/"+user.getUid()+".jpg");
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            photoBitMap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                            byte[] data = baos.toByteArray();
                            UploadTask upload = photoRef.putBytes(data);
                            upload.addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(NewUser.this, "Ocurrio un error al cargar la imagen", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    UserProfileChangeRequest updater = new UserProfileChangeRequest.Builder()
                                            .setDisplayName(nameTxt + " " + lastTxt)
                                            .setPhotoUri(taskSnapshot.getDownloadUrl())
                                            .build();
                                    user.updateProfile(updater).addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            startActivity(new Intent(getBaseContext(), Succesfull.class));
                                            finish();
                                        }
                                    });
                                }
                            });
                        }
                    }else{
                        Toast.makeText(NewUser.this, "Registro fallido.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{
            Toast.makeText(this, "Elementos invalidos.", Toast.LENGTH_SHORT).show();
        }
    }

    public void image_btn(View view){
        Log.i("Solicitud", "Solicitud de galeria");
        requestGalery();
    }

    public void camera_btn(View view){
        Log.i("Solicitud", "Solicitud de camara");
        requestCamera();
    }

    private void requestGalery(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //Explicacion permiso
                Toast.makeText(this, "Es necesario el permiso para continuar.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }else {
            actualizarImagenGaleria();
        }
    }

    private void requestCamera(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
                //Explicacion permiso
                Toast.makeText(this, "Es necesario el permiso para continuar.", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 2);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 2);
            }
        }else {
            actualizarImagenCamara();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permiso concedido
                    actualizarImagenGaleria();
                } else {
                    //permiso denegado
                    //usarPlaceholder();
                }
                return;
            }
            case 2:{
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permiso concedido
                    actualizarImagenCamara();
                } else {
                    //permiso denegado
                    //usarPlaceholder();
                }
            }
        }
    }

    private void actualizarImagenGaleria() {
        Intent pickImage = new Intent(Intent.ACTION_PICK);
        pickImage.setType("image/*");
        startActivityForResult(pickImage, 1);
    }

    private void actualizarImagenCamara() {
        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePicture, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 1:{
                if(resultCode==RESULT_OK){
                    try{
                        final InputStream imageStream = getContentResolver().openInputStream(data.getData());
                        photoBitMap = BitmapFactory.decodeStream(imageStream);
                        img.setImageBitmap(photoBitMap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case 2:{
                if(resultCode==RESULT_OK){
                    Bundle extras = data.getExtras();
                    photoBitMap = (Bitmap) extras.get("data");
                    img.setImageBitmap(photoBitMap);
                }
                break;
            }
        }
    }
}
